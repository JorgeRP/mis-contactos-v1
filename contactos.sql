CREATE DATABASE mis_contactos;

USE mis_contactos;

CREATE TABLE contactos(
	email varchar(50) NOT NULL,
	nombre varchar(50) NOT NULL, 
	sexo CHAR(1),
	nacimiento DATE,
	telefono VARCHAR(13),
	pais VARCHAR(50) NOT NULL,
	imagen VARCHAR(50),
	PRIMARY KEY (email),
	FULLTEXT KEY buscador(email, nombre, sexo, telefono, pais)
) ENGINE = MyISAM ;

CREATE TABLE pais(
	id_pais INT NOT NULL AUTO_INCREMENT,
	pais VARCHAR(50) NOT NULL,
	PRIMARY KEY (id_pais)
) ENGINE = MyISAM ;

INSERT INTO pais (id_pais, pais) VALUES
	(1,"España"),
	(2,"México"),
	(3,"Chile"),
	(4,"Argentina"),
	(5,"Brasil"),
	(6,"Angola"),
	(7,"Guinea");