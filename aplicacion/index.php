<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

$op = $_GET["op"];

switch ($op) {
	case 'alta':
		$contenido = "alta-contacto.php";
		$titulo = "Alta de Contactos";
		break;

	case 'baja':
		$contenido = "baja-contacto.php";
		$titulo = "Baja de Contactos";
		break;

	case 'cambios':
		$contenido = "cambios-contacto.php";
		$titulo = "Cambios a Contactos";
		break;

	case 'consulta':
		$contenido = "consultas-contacto.php";
		$titulo = "Consultas a Contactos";
		break;
	
	default:
		$contenido = "home.php";
		$titulo = "Mis Contactos v1";
		break;
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8"/>
	<title><?php echo $titulo ?></title>
	<link rel="stylesheet" href="mis-contactos.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script>
		!window.jQuery && document.write("<script src='jquery.min.js'><\/script>");
	</script>
	<script src="mis-contactos.js"></script>
</head>
<body>
	<section id="contenido">
		<nav>
			<ul>
				<li><a class="cambio" href="index.php">Home</a></li>
				<li><a class="cambio" href="?op=alta">Alta de Contacto</a></li>
				<li><a class="cambio" href="?op=baja">Baja de contacto</a></li>
				<li><a class="cambio" href="?op=cambios">Cambios de Contacto</a></li>
				<li><a class="cambio" href="?op=consulta">Consultas de Contacto</a></li>
			</ul>
		</nav>
		<section id="principal">
			<?php include($contenido); ?>

		</section>
	</section>


</body>

</html>