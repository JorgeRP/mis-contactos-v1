<?php 
//El parametro de $extension determina que tipo de imagen no se borrará
//La funcion file_exists evalua si un archivo existe y la funcion unlink borra un archivo del servidor

function borrar_imagenes(){

	switch($extension){
		case ".jpg":
			if(file_exists($ruta.".png")){

				unlink($ruta.".png");

			}else if(file_exists($ruta.",gif")){

				unlink($ruta.".gif");
			}
			break;

		case ".gif":
			if(file_exists($ruta.".png")){

				unlink($ruta.".png");

			}else if(file_exists($ruta.",jpg")){

				unlink($ruta.".jpg");
			}
			break;

		case ".png":
			if(file_exists($ruta.".jpg")){

				unlink($ruta.".jpg");

			}else if(file_exists($ruta.",gif")){

				unlink($ruta.".gif");
			}
			break;
	}
}

//Función para subir la imagen del perfil del usuario

function subir_imagen($tipo,$imagen,$email){
//strstr($cadena1,$cadena2) sirve paraevaluar su la primera cadena de texto existe en la segunda
//Si dentro del tipo del archivo se encuentra la palabra image significa que el archivo es una imagen
	if(strstr($tipo, "image")){
		//Para saber que tipo de extension tiene la imagen
		if(strstr($tipo, "jpeg")){
			$extension = ".jpg";
		}else if(strstr($tipo, "gif")){
			$extension = ".gif";
		}else if(strstr($tipo, "png")){
			$extension = ".png";
		}

		//para saber su la imagen tiene el ancho correcto (que es 420px)

		$tamaño_img = getimagesize($imagen);
		$ancho_img = $tamaño_img[0];
		$alto_img = $tamaño_img[1];

		$ancho_img_req = 420;

		//Si el ancho de la imagen es mayor a 420px, se reajusta su tamaño

		if($ancho_img>$ancho_img_req){

			//Por una regla de 3 obtengo el alto de la imagen de manera proporcional al ancho nuevo, que será 420px

			$nuevo_ancho_img = $ancho_img_req;
			$nuevo_alto_img = ($alto_img/$ancho_img)*$nuevo_ancho_img;

			//Creo una imagen en color real con las nuevas dimensiones

			$img_reajustada = imagecreatetruecolor($nuevo_ancho_img, $nuevo_alto_img);

			//Creo una imagen basada en la original, en base a su extensión

			switch($extension){
				case ".jpg":
					$img_original = imagecreatefromjpeg($imagen);
					//Reajusto la imagen nueva con respecto a la original
					imagecopyresampled($img_reajustada, $img_original, 0, 0, 0, 0, $nuevo_ancho_img, $nuevo_alto_img, $ancho_img, $alto_img);
					//Guardo la imagen reescalada en el servidor
					$nombre_img_ext = "archivos".$email.$extension;
					$nombre_img = "archivos".$email;
					imagejpeg($img_reajustada,$nombre_img_ext,100);
					//ejecuto la función para borrar posibles imágenes dobles
					borrar_imagenes($nombre_img,".jpg");
					break;

				case ".gif":
					$img_original = imagecreatefromgif($imagen);
					//Reajusto la imagen nueva con respecto a la original
					imagecopyresampled($img_reajustada, $img_original, 0, 0, 0, 0, $nuevo_ancho_img, $nuevo_alto_img, $ancho_img, $alto_img);
					//Guardo la imagen reescalada en el servidor
					$nombre_img_ext = "archivos".$email.$extension;
					$nombre_img = "archivos".$email;
					imagegif($img_reajustada,$nombre_img_ext,9);
					//ejecuto la función para borrar posibles imágenes dobles
					borrar_imagenes($nombre_img,".gif");
					break;

				case ".png":
					$img_original = imagecreatefrompng($imagen);
					//Reajusto la imagen nueva con respecto a la original
					imagecopyresampled($img_reajustada, $img_original, 0, 0, 0, 0, $nuevo_ancho_img, $nuevo_alto_img, $ancho_img, $alto_img);
					//Guardo la imagen reescalada en el servidor
					$nombre_img_ext = "archivos".$email.$extension;
					$nombre_img = "archivos".$email;
					imagepng($img_reajustada,$nombre_img_ext,9);
					//ejecuto la función para borrar posibles imágenes dobles
					borrar_imagenes($nombre_img,".png");
					break;		

			}

		}else{
			//No se reajusta y se sube
			$destino = "archivos".$email.$extension;

			//Se sube la imagen

			move_uploaded_file($imagen, $destino) or die("No se pudo subir la imagen al servidor");

			//Ejecuto la función para borrar posibles imágenes dobles
			$nombre_img = $email;
			borrar_imagenes($nombre_img,$extension);
		}

		//Asigno el nombre de la foto que se guardará en la BD como cadena de texto

		$imagen = $email.$extension;
		return $imagen;


	}else{

		return false;
	}
}
?>