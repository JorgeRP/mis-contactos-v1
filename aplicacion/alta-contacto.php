<form id="alta-contacto" name="alta_frm" action="agregar-contacto.php" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend>Alta de Contacto</legend>
		<div>
			<label for="email">Email: </label>
			<input type="email" id="email" class="cambio" name="email_txt" placeholder="Escribe tu email" title="Tu correo electrónico" required />
		</div>
		<div>
			<label for="nombre">Nombre: </label>
			<input type="text" id="nombre" class="cambio" name="nombre_txt" placeholder="Escribe tu nombre" title="Tu nombre" required />
		</div>
		<div>
			<label for="m">Sexo: </label>
			<input type="radio" id="m" name="sexo_rdo" title="Tu sexo" value="M" required />&nbsp;<label for="m">Masculino</label>
			&nbsp;&nbsp;&nbsp;
			<input type="radio" id="f" name="sexo_rdo" title="Tu sexo" value="F" required />&nbsp;<label for="f">Femenino</label>
		</div>
		<div>
			<label for="nacimiento">Fecha de nacimiento: </label>
			<input type="date" id="nacimiento" class="cambio" name="nacimiento_txt" title="Tu fecha de nacimiento" required />
		</div>
		<div>
			<label for="telefono">Teléfono: </label>
			<input type="number" id="telefono" class="cambio" name="telefono_txt" placeholder="Escribe tu teléfono" title="Tu número de teléfono" required />
		</div>
		<div>
			<label for="pais">Pais: </label>
			<select id="pais" class="cambio" name="pais_slc">
				<option value="">---</option>
				<?php include("select-pais.php"); ?>
			</select>
		</div>
		<div>
			<label for="avatar">Foto: </label>
			<input type="file" id="avatar" name="avatar_fls" title="Sube tu foto" />
		</div>
		&nbsp;
		<div>
			<input type="submit" id="enviar-alta" class="cambio" name="enviar_btn" value="Agregar"/>
		</div>
		<?php include("mensajes.php") ?>
	</fieldset>

</form>