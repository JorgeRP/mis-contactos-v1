<div>
	<label for="email">Email: </label>
	<input type="email" id="email" class="cambio" name="email_txt" placeholder="Escribe tu email" title="Tu correo electrónico" value="<?php echo $reg_contact["email"] ?>" disabled required />
	<input type="hidden" name="email_hdn" value="<?php echo $reg_contact["email"] ?>" />
</div>
<div>
	<label for="nombre">Nombre: </label>
	<input type="text" id="nombre" class="cambio" name="nombre_txt" placeholder="Escribe tu nombre" title="Tu nombre" value="<?php echo $reg_contact["nombre"] ?>" required />
</div>
<div>
	<label for="m">Sexo: </label>
	<input type="radio" id="m" name="sexo_rdo" title="Tu sexo" value="M" <?php if($reg_contact["sexo"]=="M"){ echo "checked"; } ?> required />&nbsp;<label for="m">Masculino</label>
	&nbsp;&nbsp;&nbsp;
	<input type="radio" id="f" name="sexo_rdo" title="Tu sexo" value="F" <?php if($reg_contact["sexo"]=="F"){ echo "checked"; } ?> required />&nbsp;<label for="f">Femenino</label>
</div>
<div>
	<label for="nacimiento">Fecha de nacimiento: </label>
	<input type="date" id="nacimiento" class="cambio" name="nacimiento_txt" title="Tu fecha de nacimiento" value="<?php echo $reg_contact["nacimiento"] ?>" required />
</div>
<div>
	<label for="telefono">Teléfono: </label>
	<input type="number" id="telefono" class="cambio" name="telefono_txt" placeholder="Escribe tu teléfono" title="Tu número de teléfono" value="<?php echo $reg_contact["telefono"] ?>" required />
</div>
<div>
	<label for="pais">Pais: </label>
	<select id="pais" class="cambio" name="pais_slc">
		<option value="">---</option>
		<?php include("select-pais.php"); ?>
	</select>
</div>
<div>
	<label for="avatar">Foto: </label>
	<input type="file" id="avatar" name="avatar_fls" title="Sube tu foto" />
	<input type="hidden" name="avatar_hdn" value="<?php echo $reg_contact["imagen"]; ?>" />
	<div>
		<img src="<?php echo "archivos".$reg_contact["imagen"]; ?>">
	</div>
</div>
&nbsp;
<div>
	<input type="submit" id="enviar-alta" class="cambio" name="enviar_btn" value="Modificar"/>
</div>