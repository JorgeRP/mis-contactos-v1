<?php 
//Asigno a variables de PHP los valores que vienen del formulario

$email = $_POST["email_txt"];
$nombre = $_POST["nombre_txt"];
$sexo = $_POST["sexo_rdo"];
$nacimiento = $_POST["nacimiento_txt"];
$telefono = $_POST["telefono_txt"];
$pais = $_POST["pais_slc"];

//Dependiendo del sexo se asigna una imagen u otra por defecto con un operador ternario (condicion)?true:false
$imagen_generica = ($sexo=="M")?"archivoshombre.jpg":"archivosmujer.jpg";

//verificamos que no exista el email del usuario registrado en la base de datos

include("conexion.php");
$consulta = "SELECT * FROM contactos WHERE email='$email'";
$ejecutar_consulta = $conexion->query($consulta);
$num_regs = $ejecutar_consulta->num_rows;

//Si $num_regs es igual a 0, el usuario no existe y por tanto insertamos los datos en la tabla. En caso contrario, mandamos un mensaje que el usuario ya existe

if($num_regs==0){
	//se ejecuta la función para subir la imagen
	include("funciones.php");
	$tipo = $_FILES["avatar_fls"]["type"];
	$archivo = $_FILES["avatar_fls"]["tmp_name"];
	//subir_imagen() necesita tres parámetros: tipo ($tipo), archivo($archivo) y email($email)
	$subir_imagen = subir_imagen($tipo,$archivo,$email);

	//Si la imagen en el formulario viene vacía, se asigna la imagen por defecto. En caso contrario, se asigna el nombre de la imagen subida.

	$imagen = empty($archivo)?$imagen_generica:$subir_imagen;

	$consulta = "INSERT INTO contactos(email, nombre, sexo, nacimiento, telefono, pais, imagen) VALUES ('$email','$nombre','$sexo','$nacimiento','$telefono','$pais','$imagen')";

	$ejecutar_consulta = $conexion->query(utf8_encode($consulta));

	if($ejecutar_consulta){

		$mensaje = "El usuario ha sido registrado<br/>";
	}else{

		$mensaje = "No se pudo dar de alta al usuario con el email <b>$email</b><br/>";
	}

}else{
	$mensaje = "Ya hay un usuario registrado con email <b>$email</b><br/>";
}

$conexion->close();
header("Location: index.php?op=alta&mensaje=$mensaje");
?>